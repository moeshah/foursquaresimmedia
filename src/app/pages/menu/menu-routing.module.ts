import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MenuPage } from './menu.page';

const routes: Routes = [
  {
    path: 'menu',
    component: MenuPage,
    children: [
      {
        path: 'home',
        loadChildren: () =>
          import('../home/home.module').then((m) => m.HomePageModule),
      },
      {
        path: 'categoryitems',
        loadChildren: () =>
          import('../categoryitems/categoryitems.module').then(
            (m) => m.CategoryitemsPageModule
          ),
      },
      {
        path: 'venuedetails',
        loadChildren: () =>
          import('../venuedetails/venuedetails.module').then(
            (m) => m.VenuedetailsPageModule
          ),
      },
      {
        path: 'googlemap',
        loadChildren: () =>
          import('../googlemap/googlemap.module').then(
            (m) => m.GooglemapPageModule
          ),
      },
    ],
  },
  {
    path: '',
    redirectTo: '/menu/home',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MenuPageRoutingModule {}

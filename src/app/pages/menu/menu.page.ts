import { Component, OnInit } from '@angular/core';
import { Router, RouterEvent } from '@angular/router';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {
  pages = [
    {
      title: 'Home',
      url: '/menu/home',
      icon: 'home-outline'
    }
  ];

  selectedPath = '';
  appversion;
  constructor(private router: Router, private appVersion: AppVersion, private menu: MenuController) {
    this.router.events.subscribe((event: RouterEvent) => {
      this.selectedPath = event.url;
    });
  }

  ngOnInit() {
    this.disableSwipe();
    this.appVersion.getVersionNumber().then(
      versionNumber => {
        console.log(versionNumber);
        this.appversion = versionNumber;
      },
      error => {
        console.log(error);
      }
    );
  }

  public disableSwipe(): void {
    this.menu.swipeGesture(false);
  }

}

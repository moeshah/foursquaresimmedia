import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { VenuedetailsPage } from './venuedetails.page';

describe('VenuedetailsPage', () => {
  let component: VenuedetailsPage;
  let fixture: ComponentFixture<VenuedetailsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VenuedetailsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(VenuedetailsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

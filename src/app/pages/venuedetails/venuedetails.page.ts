import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingController, ModalController } from '@ionic/angular';
import { FoursquaresService } from 'src/app/service/foursquares.service';
import { ImagemodalPage } from '../imagemodal/imagemodal.page';
import { LaunchNavigator } from '@ionic-native/launch-navigator/ngx';
import { GeolocationService } from 'src/app/service/geolocation.service';

@Component({
  selector: 'app-venuedetails',
  templateUrl: './venuedetails.page.html',
  styleUrls: ['./venuedetails.page.scss'],
})
export class VenuedetailsPage implements OnInit {
  ishidden = false;
  venueid;
  venimages;
  sliderOpts = {
    zoom: false,
    slidesPerView: 1,
    centeredSlides: true,
    spaceBetween: 5,
  };
  vendetails;
  lat;
  long;
  constructor(
    private router: Router,
    private loadingCtrl: LoadingController,
    private foursquares: FoursquaresService,
    private modalcontroller: ModalController,
    private launchnavigator: LaunchNavigator,
    private geolocation: GeolocationService
  ) {
    if (this.router.getCurrentNavigation().extras.state) {
      this.venueid = this.router.getCurrentNavigation().extras.state.venueId;
    }
  }

  ngOnInit() {
    this.getsinglevenuedetails();
  }

  openPreview(img, name, platform, datecreated) {
    const details =
      name + ' Via ' + platform + ' @ ' + this.convertTimestamp(datecreated);
    this.modalcontroller
      .create({
        component: ImagemodalPage,
        componentProps: {
          img,
          details,
        },
      })
      .then((modal) => modal.present());
  }

  convertTimestamp(timestamp) {
    let d = new Date(timestamp * 1000),
      yyyy = d.getFullYear(),
      mm = ('0' + (d.getMonth() + 1)).slice(-2),
      dd = ('0' + d.getDate()).slice(-2),
      hh = d.getHours(),
      h = hh,
      ampm = 'AM';
    if (hh > 12) {
      h = hh - 12;
      ampm = 'PM';
    } else if (hh === 12) {
      h = 12;
      ampm = 'PM';
    } else if (hh === 0) {
      h = 12;
    }
    const time = mm + '-' + dd + '-' + yyyy;
    return time;
  }

  async getsinglevenuedetails() {
    const loader = await this.loadingCtrl.create({
      message: 'Loading...',
      spinner: 'dots',
      cssClass: 'custom-loader-class',
    });
    loader.present();
    this.foursquares.getvenudetails(this.venueid).subscribe((data) => {
      this.vendetails = data['response']['venue'];
      console.log(data['response']['venue']);
    });
    this.foursquares.getvenuesphotos(this.venueid).subscribe((data) => {
      this.venimages = data['response']['photos']['items'];
      this.ishidden = false;
      loader.dismiss();
    });
  }

  navigateMaps(lat, lng) {
    this.geolocation.startTracking();
    this.lat = this.geolocation.lat;
    this.long = this.geolocation.lng;
    console.log(this.long + ' VD - Lat: ' + this.lat);
    this.launchnavigator
      .isAppAvailable(this.launchnavigator.APP.GOOGLE_MAPS)
      .then((isAvailable) => {
        let app;
        if (isAvailable) {
          app = this.launchnavigator.APP.GOOGLE_MAPS;
        } else {
          console.warn(
            'Google Maps not available - falling back to user selection'
          );
          app = this.launchnavigator.APP.USER_SELECT;
        }
      });
    this.launchnavigator.navigate([lat, lng], {
      start: this.lat + ',' + this.long,
    });
  }

  ionViewWillLeave() {
    this.geolocation.stopTracking();
  }
}

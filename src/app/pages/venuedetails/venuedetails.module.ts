import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VenuedetailsPageRoutingModule } from './venuedetails-routing.module';

import { VenuedetailsPage } from './venuedetails.page';
import { ImagemodalPageModule } from '../imagemodal/imagemodal.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VenuedetailsPageRoutingModule,
    ImagemodalPageModule
  ],
  declarations: [VenuedetailsPage]
})
export class VenuedetailsPageModule {}

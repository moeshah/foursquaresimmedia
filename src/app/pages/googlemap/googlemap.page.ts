import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { GeolocationService } from 'src/app/service/geolocation.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-googlemap',
  templateUrl: './googlemap.page.html',
  styleUrls: ['./googlemap.page.scss'],
})
export class GooglemapPage implements OnInit {
  @ViewChild('map', { static: false }) mapElement: any;
  constructor(private geolocation: GeolocationService, private router: Router) {
    if (this.router.getCurrentNavigation().extras.state) {
      if (this.router.getCurrentNavigation().extras.state.v) {
        this.locationsArr = this.router.getCurrentNavigation().extras.state.loc;
        this.lat = this.locationsArr[3];
        this.long = this.locationsArr[4];
        this.locations.push(this.locationsArr);
      } else {
        this.locations = this.router.getCurrentNavigation().extras.state.loc;
        this.lat = this.router.getCurrentNavigation().extras.state.lat;
        this.long = this.router.getCurrentNavigation().extras.state.lng;
      }
    }
  }

  map: google.maps.Map;
  locations = [];
  locationsArr;
  lat;
  long;

  ngOnInit(): void {}

  ionViewDidEnter() {
    this.loadMap();
    this.addMarker();
    this.addMultiMarkers();
  }

  loadMap() {
      const mapProperties = {
        center: new google.maps.LatLng(+this.lat, +this.long),
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        scrollwheel: false,
        streetViewControl: false,
        maxZoom: 18,
        minZoom: 5,
        mapTypeControl: false,
        mapTypeControlOptions: {
          mapTypeIds: ['styled_map'],
        },
      };
      this.map = new google.maps.Map(
        this.mapElement.nativeElement,
        mapProperties
      );
      const styledMapType = new google.maps.StyledMapType(
        [
          {
            elementType: 'geometry',
            stylers: [
              {
                color: '#ebe3cd',
              },
            ],
          },
          {
            elementType: 'labels.text.fill',
            stylers: [
              {
                color: '#523735',
              },
            ],
          },
          {
            elementType: 'labels.text.stroke',
            stylers: [
              {
                color: '#f5f1e6',
              },
            ],
          },
          {
            featureType: 'administrative',
            elementType: 'geometry.stroke',
            stylers: [
              {
                color: '#c9b2a6',
              },
            ],
          },
          {
            featureType: 'administrative.land_parcel',
            elementType: 'geometry.stroke',
            stylers: [
              {
                color: '#dcd2be',
              },
            ],
          },
          {
            featureType: 'administrative.land_parcel',
            elementType: 'labels.text.fill',
            stylers: [
              {
                color: '#ae9e90',
              },
            ],
          },
          {
            featureType: 'landscape.natural',
            elementType: 'geometry',
            stylers: [
              {
                color: '#dfd2ae',
              },
            ],
          },
          {
            featureType: 'poi',
            elementType: 'geometry',
            stylers: [
              {
                color: '#dfd2ae',
              },
            ],
          },
          {
            featureType: 'poi',
            elementType: 'labels.text.fill',
            stylers: [
              {
                color: '#93817c',
              },
            ],
          },
          {
            featureType: 'poi.business',
            stylers: [
              {
                visibility: 'off',
              },
            ],
          },
          {
            featureType: 'poi.park',
            elementType: 'geometry.fill',
            stylers: [
              {
                color: '#a5b076',
              },
            ],
          },
          {
            featureType: 'poi.park',
            elementType: 'labels.text.fill',
            stylers: [
              {
                color: '#447530',
              },
            ],
          },
          {
            featureType: 'road',
            elementType: 'geometry',
            stylers: [
              {
                color: '#f5f1e6',
              },
            ],
          },
          {
            featureType: 'road',
            elementType: 'labels.icon',
            stylers: [
              {
                visibility: 'off',
              },
            ],
          },
          {
            featureType: 'road.arterial',
            elementType: 'geometry',
            stylers: [
              {
                color: '#fdfcf8',
              },
            ],
          },
          {
            featureType: 'road.highway',
            elementType: 'geometry',
            stylers: [
              {
                color: '#f8c967',
              },
            ],
          },
          {
            featureType: 'road.highway',
            elementType: 'geometry.stroke',
            stylers: [
              {
                color: '#e9bc62',
              },
            ],
          },
          {
            featureType: 'road.highway.controlled_access',
            elementType: 'geometry',
            stylers: [
              {
                color: '#e98d58',
              },
            ],
          },
          {
            featureType: 'road.highway.controlled_access',
            elementType: 'geometry.stroke',
            stylers: [
              {
                color: '#db8555',
              },
            ],
          },
          {
            featureType: 'road.local',
            elementType: 'labels.text.fill',
            stylers: [
              {
                color: '#806b63',
              },
            ],
          },
          {
            featureType: 'transit',
            stylers: [
              {
                visibility: 'off',
              },
            ],
          },
          {
            featureType: 'transit.line',
            elementType: 'geometry',
            stylers: [
              {
                color: '#dfd2ae',
              },
            ],
          },
          {
            featureType: 'transit.line',
            elementType: 'labels.text.fill',
            stylers: [
              {
                color: '#8f7d77',
              },
            ],
          },
          {
            featureType: 'transit.line',
            elementType: 'labels.text.stroke',
            stylers: [
              {
                color: '#ebe3cd',
              },
            ],
          },
          {
            featureType: 'transit.station',
            elementType: 'geometry',
            stylers: [
              {
                color: '#dfd2ae',
              },
            ],
          },
          {
            featureType: 'water',
            elementType: 'geometry.fill',
            stylers: [
              {
                color: '#b9d3c2',
              },
            ],
          },
          {
            featureType: 'water',
            elementType: 'geometry.stroke',
            stylers: [
              {
                color: '#7bff3c',
              },
              {
                visibility: 'on',
              },
              {
                weight: 5,
              },
            ],
          },
          {
            featureType: 'water',
            elementType: 'labels.text.fill',
            stylers: [
              {
                color: '#92998d',
              },
            ],
          },
        ],
        { name: 'Styled Map' }
      );
      this.map.mapTypes.set('styled_map', styledMapType);
      this.map.setMapTypeId('styled_map');
  }

  addMarker() {
    const marker = new google.maps.Marker({
      map: this.map,
      icon: {
        url:
          'https://quranrecorder.mandhdevelopments.co.za/api/trackerimg/gmapdefault.png',
        scaledSize: new google.maps.Size(30, 40),
      },
      animation: google.maps.Animation.DROP,
      position: this.map.getCenter(),
    });
    const content = '<p>Your Current Position</p>';
    this.addInfoWindow(marker, content);
  }

  addMultiMarkers() {
    for (let i = 0; i < this.locations.length; i++) {
      const marker = new google.maps.Marker({
        map: this.map,
        icon: {
          url:
            'https://quranrecorder.mandhdevelopments.co.za/api/trackerimg/blip.png',
          scaledSize: new google.maps.Size(35, 25),
        },
        animation: google.maps.Animation.DROP,
        position: {
          lat: +this.locations[i][1],
          lng: +this.locations[i][2],
        },
      });

      const content =
        `<div>
      <p>Name: <strong>` +
        this.locations[i][0] +
        `</strong></p></div>`;

      this.addInfoWindow(marker, content);
    }
  }

  addInfoWindow(marker, content) {
    const infoWindow = new google.maps.InfoWindow({
      content,
    });
    google.maps.event.addListener(marker, 'click', () => {
      infoWindow.open(this.map, marker);
    });
  }

  ionViewWillLeave() {
    console.log(this.map.getDiv());
  }
}

import { Component, OnInit } from '@angular/core';
import { LoadingController, Platform, AlertController } from '@ionic/angular';
import { Router, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  subscribe: any;
  ishidden = false;
  constructor(
    private loadingCtrl: LoadingController,
    private router: Router,
    public platform: Platform,
    private alertCtrl: AlertController
  ) {}

  ionViewWillEnter() {
    this.subscribe = this.platform.backButton.subscribe(() => {
      if (
        this.constructor.name === 'HomePage' ||
        this.router.url.indexOf('menu/home') >= 0
      ) {
        this.presentAlertConfirm();
      }
    });
  }

  ionViewWillLeave() {
    this.subscribe.unsubscribe();
  }

  async presentAlertConfirm() {
    const alert = await this.alertCtrl.create({
      header: 'Confirm',
      message: 'Do you want to exit the app?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          },
        },
        {
          text: 'Yes',
          handler: () => {
            navigator['app'].exitApp();
          },
        },
      ],
      cssClass: 'exitappalert',
    });

    await alert.present();
  }

  ngOnInit() {}

  catpage(id) {
    const naviExtra: NavigationExtras = {
      state: {
        id,
      },
    };
    this.router.navigate(['menu/categoryitems'], naviExtra);
  }

  eats(val) {
    if (val) {
      this.catpage('52e81612bcbc57f1066b79ff');
    } else {
      this.catpage('4d4b7105d754a06374d81259');
    }
  }
}

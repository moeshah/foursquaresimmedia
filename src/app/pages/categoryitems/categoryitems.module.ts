import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CategoryitemsPageRoutingModule } from './categoryitems-routing.module';

import { CategoryitemsPage } from './categoryitems.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CategoryitemsPageRoutingModule
  ],
  declarations: [CategoryitemsPage]
})
export class CategoryitemsPageModule {}

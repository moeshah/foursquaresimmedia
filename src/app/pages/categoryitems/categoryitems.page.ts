import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { FoursquaresService } from 'src/app/service/foursquares.service';
import { GeolocationService } from 'src/app/service/geolocation.service';

@Component({
  selector: 'app-categoryitems',
  templateUrl: './categoryitems.page.html',
  styleUrls: ['./categoryitems.page.scss'],
})
export class CategoryitemsPage implements OnInit {
  categoryid;
  ishidden = true;
  lat;
  long;
  catdata;
  alllocations;
  constructor(
    private router: Router,
    private loadingCtrl: LoadingController,
    private foursquares: FoursquaresService,
    private geolocation: GeolocationService
  ) {
    if (this.router.getCurrentNavigation().extras.state) {
      this.categoryid = this.router.getCurrentNavigation().extras.state.id;
    }
  }
  ngOnInit() {
    this.geolocation.startTracking().then((val) => {
      this.lat = this.geolocation.lat;
      this.long = this.geolocation.lng;
      this.getdata();
    });
  }

  ionViewWillLeave() {
    this.geolocation.stopTracking();
  }

  async getdata() {
    const loader = await this.loadingCtrl.create({
      message: 'Loading...',
      spinner: 'dots',
      cssClass: 'custom-loader-class',
    });
    loader.present();
    this.foursquares
      .getcategoryitems(this.categoryid, this.lat, this.long)
      .subscribe((data) => {
        this.catdata = data['response']['venues'];
        this.ishidden = false;
        loader.dismiss();
        console.log(data['response']);
      });
  }

  gotodetails(venueId) {
    const naviExtra: NavigationExtras = {
      state: {
        venueId,
      },
    };
    this.router.navigate(['menu/venuedetails'], naviExtra);
  }

  singleMap(lat, long, name) {
    const loc = [
      name,
      lat.toString(),
      long.toString(),
      this.lat,
      this.long,
    ];
    const naviExtra: NavigationExtras = {
      state: {
        v: true,
        loc,
      },
    };
    this.router.navigate(['menu/googlemap'], naviExtra);
  }

  alllocationsMap() {
    const loc = this.getalllocations();
    const lng = +this.long;
    const lat = +this.lat;
    const naviExtra: NavigationExtras = {
      state: {
        v: false,
        loc,
        lat,
        lng
      },
    };
    this.router.navigate(['menu/googlemap'], naviExtra);
  }

  getalllocations() {
    const newArr = [];
    for (let i = 0; i <= this.catdata.length; i++) {
      for (const key in this.catdata[i]) {
        if (this.catdata[i].hasOwnProperty(key)) {
          const element = this.catdata[i];
          newArr[i] = [element.name.toString(), element.location.lat.toString(), element.location.lng.toString()];
        }
      }
    }
    return newArr;
  }
}

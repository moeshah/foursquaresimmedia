import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CategoryitemsPage } from './categoryitems.page';

const routes: Routes = [
  {
    path: '',
    component: CategoryitemsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CategoryitemsPageRoutingModule {}

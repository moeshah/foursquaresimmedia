import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';

@Component({
  selector: 'app-imagemodal',
  templateUrl: './imagemodal.page.html',
  styleUrls: ['./imagemodal.page.scss'],
})
export class ImagemodalPage implements OnInit {
  @ViewChild('slider', { read: ElementRef, static: false }) slider: ElementRef;
  img: any;
  details;
  sliderOpts = {
    zoom: {
      maxRatio: 8,
    },
  };
  constructor(
    private navParams: NavParams,
    private modalcontroller: ModalController
  ) {}

  ngOnInit() {
    this.img = this.navParams.get('img');
  }

  zoom(zoomIn: boolean) {
    const zoom = this.slider.nativeElement.swiper.zoom;
    if (zoomIn) {
      zoom.in();
    } else {
      zoom.out();
    }
  }
  close() {
    this.modalcontroller.dismiss();
  }
}

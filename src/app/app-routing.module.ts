import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./pages/menu/menu.module').then( m => m.MenuPageModule)
  },
  {
    path: 'categoryitems',
    loadChildren: () => import('./pages/categoryitems/categoryitems.module').then( m => m.CategoryitemsPageModule)
  },
  {
    path: 'venuedetails',
    loadChildren: () => import('./pages/venuedetails/venuedetails.module').then( m => m.VenuedetailsPageModule)
  },
  {
    path: 'imagemodal',
    loadChildren: () => import('./pages/imagemodal/imagemodal.module').then( m => m.ImagemodalPageModule)
  },
  {
    path: 'googlemap',
    loadChildren: () => import('./pages/googlemap/googlemap.module').then( m => m.GooglemapPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }

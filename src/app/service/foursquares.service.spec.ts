import { TestBed } from '@angular/core/testing';

import { FoursquaresService } from './foursquares.service';

describe('FoursquaresService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FoursquaresService = TestBed.get(FoursquaresService);
    expect(service).toBeTruthy();
  });
});

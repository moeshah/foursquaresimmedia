import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';

@Injectable({
  providedIn: 'root',
})
export class FoursquaresService {
  // 	Different styles. Not that my work is inconsistent.
  creds;
  apiurl = `https://api.foursquare.com/v2/`;
  constructor(private httpClient: HttpClient) {
    this.creds =
      'client_id=PXKAJQNUOAN1RZ2JDGEUWPKS5WIKPXFNNJY341Y1GIMQNIK4&client_secret=5WNIB0D3FAGC0JM4Z4KTIK3KIL0B4BIIGLPJWN0KRFF4GFEQ&v=20200506';
  }

  getcategoryitems(catId, lat, long) {
    const latlng = lat + ',' + long;
    const url =
      this.apiurl +
      `venues/search?ll=${latlng}&categoryId=${catId}&${this.creds}&venuePhotos=1&radius=10000&limit=10`;
    return this.httpClient.get(url).map((res) => res);
  }

  getvenuesphotos(vId) {
    const url = this.apiurl + `venues/${vId}/photos?${this.creds}`;
    return this.httpClient.get(url).map((res) => res);
  }

  getvenudetails(vId) {
    const url = this.apiurl + `venues/${vId}?${this.creds}`;
    return this.httpClient.get(url).map((res) => res);
  }
}

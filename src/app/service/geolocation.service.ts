import { Injectable, NgZone } from "@angular/core";
import { Geolocation, Geoposition } from "@ionic-native/geolocation/ngx";
import "rxjs/add/operator/filter";
@Injectable({
  providedIn: "root",
})
export class GeolocationService {
  public lat;
  public lng;
  public watch;
  public Gmap;

  constructor(private geolocation: Geolocation, public zone: NgZone) {}

  startTracking() {
    return new Promise((resolve, reject) => {
      const options = {
        frequency: 3000,
        enableHighAccuracy: true,
      };
      this.watch = this.geolocation
        .watchPosition(options)
        .filter((p: any) => p.code === undefined)
        .subscribe((position: Geoposition) => {
          // Run update inside of Angular's zone
          this.zone.run(() => {
            this.lat = position.coords.latitude;
            this.lng = position.coords.longitude;
          });
          resolve(position);
        });
    });
  }

  stopTracking() {
    this.watch.unsubscribe();
  }
}
